package mocklistener

import "net"

type MockAddress int

func (MockAddress) Network() string {
	return "memory"
}

func (MockAddress) String() string {
	return "local"
}
func (ml *MockListener) Addr() net.Addr {
	return MockAddress(0)
}
