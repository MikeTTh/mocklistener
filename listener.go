package mocklistener

import (
	"errors"
	"net"
)

type MockListener struct {
	net.Listener

	connections chan net.Conn
	state       chan int
}

func (ml *MockListener) Accept() (net.Conn, error) {
	select {
	case conn := <-ml.connections:
		return conn, nil
	case <-ml.state:
		return nil, errors.New("listener closed")
	}
}

func (ml *MockListener) Close() error {
	close(ml.state)
	return nil
}

func (ml *MockListener) Dial(network, addr string) (net.Conn, error) {
	select {
	case <-ml.state:
		return nil, errors.New("Listener closed")
	default:
	}
	//Create an in memory transport
	serverSide, clientSide := net.Pipe()
	//Pass half to the server
	ml.connections <- serverSide
	//Return the other half to the client
	return clientSide, nil
}

func NewMockListener() *MockListener {
	return &MockListener{
		connections: make(chan net.Conn),
		state:       make(chan int),
	}
}
